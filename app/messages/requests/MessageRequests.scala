package messages.requests

import models.Message

// Send
case class SendMessageRequest(tag: String, cmd: String, body: SendMessageRequestBody) extends Request

case class SendMessageRequestBody(message:Message) extends RequestBody

// Edit
case class EditMessageRequest(tag: String, cmd:String , body: EditMessageRequestBody) extends Request

case class EditMessageRequestBody(message:Message) extends RequestBody

// Delete
case class DeleteMessageRequest(tag: String, cmd: String, body: DeleteMessageRequestBody) extends Request

case class DeleteMessageRequestBody(messageID: String) extends RequestBody

// Retrieve
case class RetrieveMessagesRequest(tag: String, cmd: String, body: RetrieveMessagesRequestBody) extends Request

case class RetrieveMessagesRequestBody(limit: Int, since: String, before: String , latestFirst : Boolean) extends  RequestBody
