package messages.requests


trait Request {
  val tag: String
  val cmd: String
  val body: RequestBody
}

trait RequestBody

