package messages.responses

import models.Message


//Send
case class SendMessageResponse(tag: String, evt: String) extends Response

//Edit
case class EditMessageResponse(tag: String, evt: String) extends Response

//Delete
case class DeleteMessageResponse(tag: String, evt: String ) extends Response

//case class DeleteMessageResponseBody extends ResponseBody

//Retrieve

case class RetrieveMessagesResponse(tag: String, evt: String, body: RetrieveMessagesResponseBody) extends Response

case class RetrieveMessagesResponseBody(messages: Array[Message], totalMessages: Int) extends ResponseBody