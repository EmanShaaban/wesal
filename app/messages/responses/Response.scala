package messages.responses


trait Response {
  val tag: String
  val evt: String
 // val body: Option[ResponseBody] = null
 // val errors: Option[Array[error]] = None

}

trait ResponseBody


case class error(code: Int, message: String)