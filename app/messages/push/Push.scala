package messages.push


trait Push {
  val alert: String
  val body: PushBody
}

trait PushBody