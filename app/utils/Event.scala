package utils


object Event {
  final val MESSAGESENT: String = "message-sent"
  final val MESSAGEEDITED: String = "message-edited"
  final val MESSAGEDELETED: String = "message-deleted"
  final val MESSAGESRETRIEVED: String = "messages-retrieved"
}
