package utils


object Command {
  final val INITIALIZE: String = "initialize"
  final val SENDMESSAGE: String = "send-message"
  final val EDITMESSAGE: String = "edit-message"
  final val DELETEMESSAGE: String = "delete-message"
  final val RETRIEVEMESSAGES: String = "retrieve-messages"

}