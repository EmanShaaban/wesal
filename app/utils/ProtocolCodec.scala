package utils

import com.fasterxml.jackson.databind.JsonNode
import controllers.Global
import messages.requests._
import messages.responses._
import models.Message
import play.api.libs.json._


import play.api.libs.functional.syntax._


object ProtocolCodec {
  def sendMessage(req: SendMessageRequest): String ={
    val res = new SendMessageResponse(req.tag,Event.MESSAGESENT)
    val out = Serializer.serialize(res)
    return out
  }

  def deleteMessage(req: DeleteMessageRequest):String ={
    val res = new DeleteMessageResponse(req.tag ,Event.MESSAGEDELETED)
    val out =Serializer.serialize(res)
    println("delmsg"+res)
    return out
  }

  def retrieveMessages (req: RetrieveMessagesRequest): String ={
    val msg1 = Message( "1","msg1")
    val msg2 = Message( "2","msg2")
    val messages = Array(msg1,msg2)
    val body = new RetrieveMessagesResponseBody(messages,50)
    val res = new RetrieveMessagesResponse(req.tag,Event.MESSAGESRETRIEVED, body)
    val out = Serializer.serialize(res)
    return out
  }

}




