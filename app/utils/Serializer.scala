package utils

import java.io.StringWriter

import controllers.Global
import messages.responses.Response


object Serializer {
  def serialize(res: Response): String = {
    val out = new StringWriter()
    Global.mapper.writeValue(out, res)
    val json = out.toString
   // print("jsooooooon" + json)
    return json
  }

}
