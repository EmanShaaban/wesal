package utils

import controllers.Global
import messages.requests._
import play.api.libs.json.JsValue


object Deserializer {

  def sendMessage(json: JsValue): SendMessageRequest = {
    val js = json.toString()
    val obj = Global.mapper.readValue(js, classOf[SendMessageRequest])
    return obj

  }

  def retrieveMessages(json: JsValue): RetrieveMessagesRequest = {
    val js = json.toString()
    val obj = Global.mapper.readValue(js, classOf[RetrieveMessagesRequest])
    return obj
  }
}
