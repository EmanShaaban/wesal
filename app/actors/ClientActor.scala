package actors


import akka.actor.{Actor, ActorRef, Props}
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ObjectNode
import messages.requests._
import play.Logger
import play.api.libs.json.JsValue
import utils._


object ClientActor {
  def props(out: ActorRef) = Props(new ClientActor(out))
}


class ClientActor(out: ActorRef) extends Actor {
  Logger.info("New ClientActor created: " + self.path)

  def replayWith(response: ObjectNode) = {
    out ! response
  }


  def receive = {
    case message: JsValue =>

      val jsonRequest = message.as[JsonNode]

      if (jsonRequest.has("cmd")) {
        val command: String = jsonRequest.get("cmd").textValue()
        command match {
          case Command.SENDMESSAGE =>
            val req = Deserializer.sendMessage(message)
            val res = ProtocolCodec.sendMessage(req)
            out ! res
          case Command.RETRIEVEMESSAGES =>
            val req = Deserializer.retrieveMessages(message)
            val res = ProtocolCodec.retrieveMessages(req)
            out ! res
        }
      }
  }
}
