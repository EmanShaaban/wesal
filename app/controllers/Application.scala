package controllers

import actors.ClientActor
import com.fasterxml.jackson.databind.{ObjectMapper, JsonNode}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import play.api._
import play.api.libs.json.{JsObject, JsValue}
import play.api.mvc._
import play.api.Play.current

object Global extends GlobalSettings{
      val mapper = new ObjectMapper()
      mapper.registerModule(DefaultScalaModule)
}
class Application extends Controller {

  def index = Action {
    Ok("I am alive :P")
  }

  def websocket = WebSocket.acceptWithActor[JsValue, String] { request => out =>
    Logger.info("New connection from " + request.remoteAddress)
    ClientActor.props(out)
  }

}
